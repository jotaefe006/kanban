<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
   
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'user_id'
    ];

    /**
     * Return relations with Model User
     */

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Return relations with Model List
     */

    public function screens() {
        return $this->hasMany('App\Models\Screen');
    }
}
