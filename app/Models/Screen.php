<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Screen extends Model
{
   
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'board_id'
    ];

     /**
     * Return relations with Model Board
     */

    public function board() {
        return $this->belongsTo('App\Models\Board');
    }

    /**
     * Return relations with Model Task
     */

    public function tasks() {
        return $this->hasMany('App\Models\Task');
    }
}
