<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
   /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'title',
        'screen_id',
        'completed',
        'dateFinish'
    ];

     /**
     * Return relations with Model Screen
     */

    public function screen() {
        return $this->belongsTo('App\Models\Screen');
    }

}
