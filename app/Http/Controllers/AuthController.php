<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Tymon\JWTAuth\Facades\JWTAuth;
use Auth;
// use Validator;

class AuthController extends Controller
{
    public function _construct(){
        $this->middleware('auth:api',['except'=>['login','register']]);
    }

    public function login(Request $request){

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if($validator->fails()) {
            return response()->json($validator->errors(),422);
        }

        if(!$token = auth()->attempt($validator->validated())){
            return response()->json(['error' => 'Unauthorized'],401);

        }
        return $this->createNewToken($token);
    }

    public function register(Request $request){
      
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|between:2,100',
            'email' => 'required|email|string|max:100|unique:users',
            'password' => 'required|confirmed|',
        ]);
        if($validator->fails()){
            return response()->json(['error' => $validator->errors()->toJson()],400);
        }

        $user = User::create(array_merge(
            $validator->validated(),
            ['password' => bcrypt($request->password)]
        ));

        if(!$token = auth()->attempt($request->only('email','password'))){
            return response()->json(['error' => 'Unauthorized'],401);
        }

        return $this->createNewToken($token);
     

    }

    protected function createNewToken($token){

        return response()->json([
            'acces_token' => $token,
            'token_type' => 'bearer',
            'user' => auth()->user(),
            'expires_in' =>auth()->factory()->getTTL()*6000,
            'success' =>true
        ]);
    }

    public function getUser(Request $request){
        return response()->json($request->user());
    }

    public function refresh(){
        return $this->createNewToken(auth()->refresh());
    }

    public function logout(){
        auth()->logout();
        return response()->json([
            'message' => 'User logged out',
        ]);
    }

    public function profile(){
        return response()->json(auth()->user());
    }

    public function checkToken(){
        return response()->json(['success'=>true],200);
    }
}
