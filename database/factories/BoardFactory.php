<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Support\Str;
use App\Models\User;


$factory->define(App\Models\Board::class, function (Faker $faker) {
    
    return [
        'name' => $this->faker->sentence($nbWords = 3, $variableNbWords = true),
        'user_id' => function () {
            return User::all()->random();
        }
    ];
});
