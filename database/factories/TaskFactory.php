<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\Models\Screen;
use App\Models\Task;

$factory->define(Task::class, function (Faker $faker) {
    return [
        'title' => $this->faker->name(),
        'screen_id' => function () {
            return Screen::first();
        }
    ];
});
