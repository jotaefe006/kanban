<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\Models\Board;
use App\Models\Screen;

$factory->define(Screen::class, function (Faker $faker) {
    return [
        'name' => $this->faker->name(),
        'board_id' => function () {
            return Board::first();
        } 
    ];
});


