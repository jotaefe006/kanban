<?php

use Illuminate\Database\Seeder;
use App\Models\Screen;

class ScreensTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Screen::class)->create([
            'name' => 'Buffer'
        ]);
        factory(Screen::class)->create([
            'name' => 'Working'
        ]);
        factory(Screen::class)->create([
            'name' => 'Done'
        ]);
        
    }
}
