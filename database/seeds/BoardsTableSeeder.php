<?php

use Illuminate\Database\Seeder;
use App\Models\Board;
use App\Models\User;

class BoardsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      
        factory(Board::class,1)->create();
        
    }
}
