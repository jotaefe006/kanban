<?php

use Illuminate\Database\Seeder;
use App\Models\Screen;
use App\Models\Task;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Task::class)->create([
            'title' => 'Agregar subtareas al kanban',
            'completed' => false,
        ]);

        factory(Task::class)->create([
            'title' => 'Testing Kanban',
            'completed' => false,
        ]);

        factory(Task::class)->create([
            'title' => 'Test fullstack',
            'completed' => false,
        ]);
     
    }
}
