import * as Vue from 'vue';
import * as VueRouter from 'vue-router';
import Home from './components/Home';
import Login from './components/Login';
import Register from './components/Register';
import Dashboard from './components/Dashboard';
import Board from './components/Board';


export const routes = [
    {
        path:'/',
        component: Home
    },
    {
        path:'/login',
        component: Login
    },
    {
        path:'/register',
        component: Register
    },
    {
        path:'/dashboard',
        component: Dashboard
    },
    {
        path:'/board/:id',
        name:'board',
        component: Board,
        props:true
    }
];

const router = VueRouter.createRouter({
  history: VueRouter.createWebHistory(),
  routes,
});

export default router;
