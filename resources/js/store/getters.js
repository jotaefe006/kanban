export default {

    /**
     * Getter: All boards user
     *
     * @param {*} state
     * @returns
     */
     boards:(state) => {
        return state.boards;
    },
    /**
     * Getter: All lists by board
     *
     * @param {*} state
     * @returns
     */
     lists:(state) => {
        return state.lists;
    },
    /**
     * Getter: All taskLists by list
     *
     * @param {*} state
     * @returns
     */
    tasksLists:(state) => {
        return state.tasksLists;
    },
    /**
     * Getter: count task by list
     *
     * @param {*} state
     * @returns
     */
     countTask:(state) => {
      return state.countTask;
  },
    
  }