import {createApp} from 'vue';
import {createStore} from 'vuex';

import * as axios from 'axios'
import state from './state';
import mutations from './mutations';
import actions from './actions';
import getters from './getters';


export const store = createStore({

	state,
	getters,
	actions,
	mutations
})