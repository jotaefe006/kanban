
export default {


    /**
     * Mutation: SET_BOARDS_USER
     *
     * @param {*} state
     * @param {*} 
     */
     SET_BOARDS_USER(state, boards){

        state.boards = {...boards}
    },

     /**
     * Mutation: SET_LISTS_BOARD
     *
     * @param {*} state
     * @param {*} 
     */
    
      SET_LISTS_BOARD(state, lists){
        state.lists = {...lists}
    },

    /**
     * Mutation: SET_TASK_LIST
     *
     * @param {*} state
     * @param {*} 
     */
     SET_TASK_LIST(state, tasksLists){
        state.tasksLists = {...tasksLists};
    },

     /**
     * Mutation: ADD_TASK
     *
     * @param {*} state
     * @param {*} 
     */
      ADD_TASK(state, task){
      
        state.lists.map(list => {
            if(list.id === task.screen_id){
                list.tasks.push({id:task.id,title:task.title,dateFinish:task.dateFinish});
            }
        })
    },
    /**
     * Mutation: COMPLETED_TASK
     *
     * @param {*} state
     * @param {*} 
     */
     COMPLETED_TASK(state, task){
        task.completed = !task.completed
    },


    /**
     * Mutation: setToken
     *
     * @param {*} state
     * @param {*} 
     */
    setToken(state,token){
        localStorage.setItem('auth',token);
        state.token = token;
    },
    /**
     * Mutation: clearToken
     *
     * @param {*} state
     * @param {*} 
     */
    clearToken(state){
        localStorage.removeItem('auth');
        state.token = '';
    }
}