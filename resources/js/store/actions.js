import axios from 'axios';

export default {

    /**
     * Action: all boards user
     * 
     * @param {*} {commit,getters} 
     */
    async getBoardsUser({ commit, getters }, user_id){
      
        await axios.post('/api/auth/boards',user_id)
            .then(res => {
                commit('SET_BOARDS_USER',res.data.data);
            }).catch(err => {
                console.log(err)
            })
    
    },

    /**
     * Action: all lists by board
     * 
     * @param {*} {commit,getters} 
     */
    async getListsBoard({ commit, getters }, board_id){
   
        localStorage.setItem('board_id',board_id.board_id);
        await axios.post('/api/auth/screens',board_id)
            .then(res => {
                commit('SET_LISTS_BOARD',res.data.data);
            }).catch(err => {
                console.log(err)
            })
    
    },

     /**
     * Action: all tasks by List
     * 
     * @param {*} {commit,getters} 
     */
    async getTaskList({ commit, getters }, list_id){
    
        await axios.post('/api/auth/tasks',list_id)
            .then(res => {
                commit('SET_TASK_LIST',res.data.data);
            }).catch(err => {
                console.log(err)
            })
    
    },
    

    /**
     * Action: all tasks by List
     * 
     * @param {*} {commit,getters} 
     */
     async saveTask({ commit, getters }, task){
    
        await axios.post('/api/auth/task',task)
            .then(res => {
                console.log(res);
            }).catch(err => {
                console.log(err)
            })
    },

    /**
     * Action: change completed task
     * 
     * @param {*} {commit,getters} 
     */
     async markAsCompleted({ commit, getters }, task){
        console.log('task',task);
        task.completed = !task.completed;
        await axios.post('/api/auth/task',task)
            .then(res => {
                commit('COMPLETED_TASK',res.data.data);
            }).catch(err => {
                console.log(err)
            })
    
    },
    /**
     * Action: move along task to list
     * 
     * @param {*} {commit,getters} 
     */
     async  moveAlong({ commit, getters }, task){

        if(task.screen_id === 1){
            task.screen_id = 2;
        } else if(task.screen_id === 2){
            task.screen_id = 3;
        }
        
        await axios.post('/api/auth/task',task)
        .then(res => {
            let new_task= res.data.data;
            new_task.old_screen = task.screen_id;
        }).catch(err => {
            console.log(err)
        })
    
    },
    /**
     * Action: move back task to list
     * 
     * @param {*} {commit,getters} 
     */
     async  moveBack({ commit, getters }, task){
        let old_task= task.screen_id;
   
        if(task.screen_id === 3){
            task.screen_id = 2;
        } else if(task.screen_id === 2){
            task.screen_id = 1;
        }
        
        await axios.post('/api/auth/task',task)
        
        .then(res => {
            let new_task = res.data.data;
            new_task.old_screen = old_task;
            commit('UPDATE_LIST_TASK',new_task);
        }).catch(err => {
            console.log(err)
        })
    
    },

    /**
     * Action: all tasks by List
     * 
     * @param {*} {commit,getters} 
     */
     async removeTask({ commit, getters }, task){
    
        await axios.post('/api/auth/tasks/delete',task)
            .then(res => {
                console.log('res',res);

            }).catch(err => {
                console.log(err)
            })
    
    },
  }