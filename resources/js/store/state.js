export default {
    user:null,
    fetchingData:true,
    error:null,
    token: localStorage.getItem('auth'),
    boards:{},
    lists:{},
    tasksLists:[],
    board_id:localStorage.getItem('board_id'),
    countTask:0,
}