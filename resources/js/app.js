require('./bootstrap');

import { createApp } from 'vue';
import router from './app/routes'
import Root from './app/components/App';
import {store} from './store/index';


// app.use(VueCookie)
// app.provide('cookies',app.config.globalProperties.$cookies);
const app = createApp(Root);

app.use(router).use(store).mount('#app')